#ifndef SINGLE_LIST_ITEM_H
#define SINGLE_LIST_ITEM_H

/*
  An item of a single linked list
*/
template <typename KeyT, typename ValueT>
class SingleListItem
{
  private:
    KeyT key_;
    ValueT value_;
    SingleListItem *next_;

  public:
    SingleListItem<KeyT, ValueT>(KeyT key,
                                 ValueT val,
                                 SingleListItem *next);

    /* Get key of this list item */
    const KeyT& get_key() const;
    /* Get value of this list item */
    const ValueT& get_value() const;
    /* Get ptr to next list item or nullptr if it doesn't exist*/
    SingleListItem* get_next() const;
    /* Set ptr to next list item */
    void set_next(SingleListItem*);
    /* Copy in data from other list item*/
    void copy_from(const SingleListItem<KeyT, ValueT> *src);
};

#include "SingleListItem-impl.h"

#endif // SINGLE_LIST_ITEM_H
