#ifndef B_S_TREE_IMPL_H
#define B_S_TREE_IMPL_H

template<typename KeyT, typename ValueT>
void BSTree<KeyT, ValueT>::insert(const KeyT &key, const ValueT &value)
{
    BSTreeNode<KeyT, ValueT> *iter = root_;
    BSTreeNode<KeyT, ValueT> *iter_parent = nullptr;

    while (iter != nullptr) {
        iter_parent = iter;
        if (key < iter->get_key()) {
            iter = iter->get_left();
        } else if (key > iter->get_key()) {
            iter = iter->get_right();
        } else {
            // Found existing node: set new value
            iter->set_value(value);
            return;
        }
    }

    if (root_ == nullptr) {
        root_ = new BSTreeNode<KeyT, ValueT>(key, value);
        return;
    }

    // Now iter_parent is the parent of the new node
    auto new_node = new BSTreeNode<KeyT, ValueT>(key, value, iter_parent, nullptr, nullptr);
    if (key < iter_parent->get_key()) {
        iter_parent->set_left(new_node);
    } else {
        iter_parent->set_right(new_node);
    }
}

template<typename KeyT, typename ValueT>
void BSTree<KeyT, ValueT>::erase(const KeyT &key)
{
    BSTreeNode<KeyT, ValueT> *iter = root_;
    while (iter != nullptr) {
        if (key < iter->get_key()) {
            iter = iter->get_left();
        } else if (key > iter->get_key()) {
            iter = iter->get_right();
        } else {
            break;
        }
    }

    if (iter->get_left() == nullptr and iter->get_right() == nullptr) {
        
    }
}

template<typename KeyT, typename ValueT>
bool BSTree<KeyT, ValueT>::has_key(const KeyT &key) const
{
    BSTreeNode<KeyT, ValueT> *iter = root_;
    while (iter != nullptr) {
        if (key < iter->get_key()) {
            iter = iter->get_left();
        } else if (key > iter->get_key()) {
            iter = iter->get_right();
        } else {
            return true;
        }
    }
    return false;
}

template<typename KeyT, typename ValueT>
const ValueT& BSTree<KeyT, ValueT>::get(const KeyT &key) const
{
    BSTreeNode<KeyT, ValueT> *iter = root_;
    while (iter != nullptr) {
        if (key < iter->get_key()) {
            iter = iter->get_left();
        } else if (key > iter->get_key()) {
            iter = iter->get_right();
        } else {
            return iter->get_value();
        }
    }
}

template<typename KeyT, typename ValueT>
BSTreeNode<KeyT, ValueT>* BSTree<KeyT, ValueT>::get_root() const
{
    return root_;
}

template<typename KeyT, typename ValueT>
void BSTree<KeyT, ValueT>::print(BSTreeNode<KeyT, ValueT> *root)
{
    if (root == nullptr) return;

    print(root->get_left());
    std::cout << root->get_key() << " " << root->get_value() << std::endl;
    print(root->get_right());
}


#endif // B_S_TREE_IMPL_H
