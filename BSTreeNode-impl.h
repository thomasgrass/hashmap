#ifndef B_S_TREE_NODE_IMPL_H
#define B_S_TREE_NODE_IMPL_H

template<typename KeyT, typename ValueT>
BSTreeNode<KeyT, ValueT>::BSTreeNode(KeyT key, ValueT value)
    : key_(key), value_(value)
{

}

template<typename KeyT, typename ValueT>
BSTreeNode<KeyT, ValueT>::BSTreeNode(KeyT key, ValueT value, BSTreeNode *parent,
                                         BSTreeNode *left, BSTreeNode *right)
    : key_(key), value_(value), parent_(parent), left_(left), right_(right)
{

}

template<typename KeyT, typename ValueT>
inline
const KeyT& BSTreeNode<KeyT, ValueT>::get_key() const
{
    return key_;
}

template<typename KeyT, typename ValueT>
inline
const ValueT& BSTreeNode<KeyT, ValueT>::get_value() const
{
    return value_;
}

template<typename KeyT, typename ValueT>
inline
BSTreeNode<KeyT, ValueT>* BSTreeNode<KeyT, ValueT>::get_parent() const
{
    return parent_;
}

template<typename KeyT, typename ValueT>
inline
BSTreeNode<KeyT, ValueT>* BSTreeNode<KeyT, ValueT>::get_left() const
{
    return left_;
}

template<typename KeyT, typename ValueT>
inline
BSTreeNode<KeyT, ValueT>* BSTreeNode<KeyT, ValueT>::get_right() const
{
    return right_;
}

template<typename KeyT, typename ValueT>
inline
void BSTreeNode<KeyT, ValueT>::set_key(const KeyT &key)
{
    key_ = key;
}

template<typename KeyT, typename ValueT>
inline
void BSTreeNode<KeyT, ValueT>::set_value(const ValueT &value)
{
    value_ = value;
}

template<typename KeyT, typename ValueT>
inline
void BSTreeNode<KeyT, ValueT>::set_parent(BSTreeNode<KeyT, ValueT> *parent)
{
    parent_ = parent;
}

template<typename KeyT, typename ValueT>
inline
void BSTreeNode<KeyT, ValueT>::set_left(BSTreeNode<KeyT, ValueT> *left)
{
    left_ = left;
}

template<typename KeyT, typename ValueT>
inline
void BSTreeNode<KeyT, ValueT>::set_right(BSTreeNode<KeyT, ValueT> *right)
{
    right_ = right;
}


#endif // B_S_TREE_NODE_IMPL_H
