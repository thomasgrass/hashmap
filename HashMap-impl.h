#ifndef HASH_MAP_IMPL_H
#define HASH_MAP_IMPL_H

template <typename KeyT, typename ValueT, template <typename, typename> class LinkedListT>
size_t HashMap<KeyT, ValueT, LinkedListT>::hash(const KeyT &key)
{
    return key % num_bins_;
}

template <typename KeyT, typename ValueT, template <typename, typename> class LinkedListT>
HashMap<KeyT, ValueT, LinkedListT>::HashMap()
{
    bins_ = new LinkedListT<KeyT, ValueT>[num_bins_];
}

template <typename KeyT, typename ValueT, template <typename, typename> class LinkedListT>
HashMap<KeyT, ValueT, LinkedListT>::~HashMap()
{
    delete[] bins_;
}

template <typename KeyT, typename ValueT, template <typename, typename> class LinkedListT>
void HashMap<KeyT, ValueT, LinkedListT>::insert(const KeyT &key, const ValueT &val)
{
    size_t hashed_key = hash(key);
    bins_[hashed_key].insert(key, val);
}

template <typename KeyT, typename ValueT, template <typename, typename> class LinkedListT>
bool HashMap<KeyT, ValueT, LinkedListT>::has_key(const KeyT &key)
{
   size_t hashed_key = hash(key);
   if (bins_[hashed_key] != nullptr) {
       return bins_[hashed_key]->has_key(key);
   }
   return false;
}

template <typename KeyT, typename ValueT, template <typename, typename> class LinkedListT>
const ValueT& HashMap<KeyT, ValueT, LinkedListT>::get(const KeyT &key)
{
    size_t hashed_key = hash(key);
    return bins_[hashed_key]->get(key);
}

template <typename KeyT, typename ValueT, template <typename, typename> class LinkedListT>
void HashMap<KeyT, ValueT, LinkedListT>::erase(const KeyT &key)
{
    size_t hashed_key = hash(key);
    bins_[hashed_key].erase(key);
}


#endif // HASH_MAP_IMPL_H
