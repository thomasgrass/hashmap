#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>

#include "SingleLinkedList.h"
#include "HashMap.h"

int main()
{
    std::chrono::high_resolution_clock::time_point t1;
    std::chrono::high_resolution_clock::time_point t2;
    std::chrono::high_resolution_clock::time_point t3;
    std::chrono::high_resolution_clock::time_point t4;

    HashMap<unsigned, int, SingleLinkedList> hash_map;

    // Delete in insertion order
    std::cout << "Delete in order of insertion" << std::endl;
    std::cout << "NumElements:InsertTime:DeleteTime" << std::endl;
    for (int exp_size = 100000; exp_size <= 1000000; exp_size += 100000) {
        t1 = std::chrono::high_resolution_clock::now();
        for (int i = 0; i < exp_size; i++) {
            hash_map.insert(i, 2*i);
        }
        t2 = std::chrono::high_resolution_clock::now();
    
        t3 = std::chrono::high_resolution_clock::now();
        // Delete elements in insertion order
        for (int i = 0; i < exp_size; i++) {
            hash_map.erase(i);
        }
        t4 = std::chrono::high_resolution_clock::now();

        auto insert_time = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        auto erase_time = std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count();
        std::cout << exp_size << ":" << insert_time << ":" << erase_time << std::endl;
 
    }

    // Delete in insertion order
    std::cout << "Delete in reverse insertion order" << std::endl;
    std::cout << "NumElements:InsertTime:DeleteTime" << std::endl;
    for (int exp_size = 100000; exp_size <= 1000000; exp_size += 100000) {
        t1 = std::chrono::high_resolution_clock::now();
        // Insert elements
        for (int i = 0; i < exp_size; i++) {
            hash_map.insert(i, 2*i);
        }
        t2 = std::chrono::high_resolution_clock::now();

        t3 = std::chrono::high_resolution_clock::now();
        // Delete elements in reverse insertion order
        for (int i = 0; i < exp_size; i++) {
            hash_map.erase(exp_size - i);
        }
        t4 = std::chrono::high_resolution_clock::now();

        auto insert_time = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        auto erase_time = std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count();
        std::cout << exp_size << ":" << insert_time << ":" << erase_time << std::endl;

    }

    // Insert and delete in random order
    std::cout << "Random insertion and deletion order" << std::endl;
    std::cout << "NumElements:InsertTime:DeleteTime" << std::endl;
    for (int exp_size = 100000; exp_size <= 1000000; exp_size += 100000) {
        std::vector<int> keys(exp_size);
        for (int i = 0; i < exp_size; ++i) {
            keys[i] = i;
        }

        std::random_shuffle(keys.begin(), keys.end());
        t1 = std::chrono::high_resolution_clock::now();
        // Insert elements in random order
        for (int i = 0; i < exp_size; i++) {
            hash_map.insert(keys[i], 2*i);
        }
        t2 = std::chrono::high_resolution_clock::now();

        std::random_shuffle(keys.begin(), keys.end());
        t3 = std::chrono::high_resolution_clock::now();
        // Delete elements in random order
        for (int i = 0; i < exp_size; i++) {
            hash_map.erase(keys[i]);
        }
        t4 = std::chrono::high_resolution_clock::now();

        auto insert_time = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        auto erase_time = std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count();
        std::cout << exp_size << ":" << insert_time << ":" << erase_time << std::endl;

    }
}
