

all:
	g++ -std=c++11 -O3 hashmap_test.cpp -o hashmap_test

run:
	./hashmap_test

run-valgrind:
	valgrind --leak-check=full -v ./hashmap_test

clean:
	rm -f hashmap_test
