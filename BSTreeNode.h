#ifndef B_S_TREE_NODE_H
#define B_S_TREE_NODE_H

template<typename KeyT, typename ValueT>
class BSTreeNode
{
  private:
    KeyT key_;
    ValueT value_;
    BSTreeNode *parent_ = nullptr;
    BSTreeNode *left_ = nullptr;
    BSTreeNode *right_ = nullptr;

  public:
    BSTreeNode<KeyT, ValueT>(KeyT key, ValueT value);
    BSTreeNode<KeyT, ValueT>(KeyT key, ValueT value, BSTreeNode *parent,
                             BSTreeNode *left, BSTreeNode *right);
    const KeyT& get_key() const;
    const ValueT& get_value() const;
    BSTreeNode* get_parent() const;
    BSTreeNode* get_left() const;
    BSTreeNode* get_right() const;
    void set_key(const KeyT &key);
    void set_value(const ValueT &value);
    void set_parent(BSTreeNode *parent);
    void set_left(BSTreeNode *left);
    void set_right(BSTreeNode *right);
};

#include "BSTreeNode-impl.h"

#endif // B_S_TREE_NODE_H
