#ifndef SINGLE_LINKED_LIST_H
#define SINGLE_LINKED_LIST_H

#include "SingleListItem.h"

/*
  A single linked list
*/
template <typename KeyT, typename ValueT>
class SingleLinkedList
{
  private:
    SingleListItem<KeyT, ValueT> *head_ = nullptr;
    const SingleListItem<KeyT, ValueT>* search(const KeyT &key) const;
    /* Get pointer to list item prior to tail */
    SingleListItem<KeyT, ValueT>* search_prev_tail() const;

  public:
    SingleLinkedList();
    virtual ~SingleLinkedList();

    /* Insert new item to head of list */
    void insert(const KeyT &key, const ValueT &value);
    /* Erase list item by key */
    void erase(const KeyT &key);
    /* Check if list has item with given key */
    bool has_key(const KeyT &key);
    /* Get value of list item with given key */
    const ValueT &get(const KeyT &key) const;
};

#include "SingleLinkedList-impl.h"

#endif // SINGLE_LINKED_LIST_H
