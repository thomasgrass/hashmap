#ifndef HASH_MAP_H
#define HASH_MAP_H

/*
  A hash map for storing (key, value) pairs of types KeyT and ValueT. Has only
  been testet with integer key types.
*/
template <typename KeyT, typename ValueT, template <typename, typename> class LinkedListT>
class HashMap
{
  private:
    static const unsigned num_bins_ = 1024 * 1024;
    LinkedListT<KeyT, ValueT> *bins_;

    size_t hash(const KeyT &val);

  public:
    HashMap();
    virtual ~HashMap();

    /* Insert element */
    void insert(const KeyT &key, const ValueT &val);
    /* Check if hash map has element with given key */
    bool has_key(const KeyT &key);
    /* Get value of element with given key, does not check if element exists */
    const ValueT& get(const KeyT &key);
    /* Erase element with given key */
    void erase(const KeyT &key);
};

#include "HashMap-impl.h"

#endif // HASH_MAP_H
