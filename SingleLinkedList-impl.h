#ifndef SINGLE_LINKED_LIST_IMPL_H
#define SINGLE_LINKED_LIST_IMPL_H

template <typename KeyT, typename ValueT>
SingleLinkedList<KeyT, ValueT>::SingleLinkedList()
{

}

template <typename KeyT, typename ValueT>
SingleLinkedList<KeyT, ValueT>::~SingleLinkedList()
{
    if (head_ == nullptr) {
        return;
    }

    SingleListItem<KeyT, ValueT> *prev;
    while (head_->get_next() != nullptr) {
        prev = head_;
        head_ = head_->get_next();
        delete prev;
    }
    delete head_;
}

template <typename KeyT, typename ValueT>
void SingleLinkedList<KeyT, ValueT>::insert(const KeyT &key, const ValueT &value)
{
    if (head_ == nullptr) {
        head_ = new SingleListItem<KeyT, ValueT>(key, value, nullptr);
    } else {
        head_ = new SingleListItem<KeyT, ValueT>(key, value, head_);
    }
}

template <typename KeyT, typename ValueT>
const SingleListItem<KeyT, ValueT>* SingleLinkedList<KeyT, ValueT>::search(const KeyT &key) const
{
    SingleListItem<KeyT, ValueT> *iter = head_;
    while (iter != nullptr) {
        if (iter->get_key() == key) {
            return iter;
        }
        iter = iter->get_next();
    }
    return nullptr;
}

template <typename KeyT, typename ValueT>
SingleListItem<KeyT, ValueT>* SingleLinkedList<KeyT, ValueT>::search_prev_tail() const
{
    SingleListItem<KeyT, ValueT> *iter = head_;
    while (iter->get_next()->get_next() != nullptr) {
        iter = iter->get_next();
    }
    return iter;
}

template <typename KeyT, typename ValueT>
void SingleLinkedList<KeyT, ValueT>::erase(const KeyT &key)
{
    SingleListItem<KeyT, ValueT> *result = const_cast<SingleListItem<KeyT, ValueT>*>(search(key));
    if (result != nullptr) {
        if (result == head_) {
            SingleListItem<KeyT, ValueT> *new_head = head_->get_next();
            delete head_;
            head_ = new_head;
        } else if (result->get_next() == nullptr) {
            SingleListItem<KeyT, ValueT> *new_tail = search_prev_tail();
            new_tail->set_next(nullptr);
            delete result;
        } else {
            SingleListItem<KeyT, ValueT> *delete_me = result->get_next();
            result->copy_from(result->get_next());
            delete delete_me;
        }
    }
}

template <typename KeyT, typename ValueT>
bool SingleLinkedList<KeyT, ValueT>::has_key(const KeyT &key)
{
    if (search(key) != nullptr) {
        return true;
    }
    return false;
}

template <typename KeyT, typename ValueT>
const ValueT& SingleLinkedList<KeyT, ValueT>::get(const KeyT &key) const
{
    return search(key)->get_value();
}

#endif // SINGLE_LINKED_LIST_IMPL_H
