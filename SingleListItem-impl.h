#ifndef SINGLE_LIST_ITEM_IMPL_H
#define SINGLE_LIST_ITEM_IMPL_H

template <typename KeyT, typename ValueT>
SingleListItem<KeyT, ValueT>::SingleListItem(KeyT key,
                                             ValueT value,
                                             SingleListItem *next)
        : key_(key), value_(value), next_(next)
{

}

template <typename KeyT, typename ValueT>
const KeyT& SingleListItem<KeyT, ValueT>::get_key() const
{
    return key_;
}

template <typename KeyT, typename ValueT>
const ValueT& SingleListItem<KeyT, ValueT>::get_value() const
{
    return value_;
}

template <typename KeyT, typename ValueT>
SingleListItem<KeyT, ValueT>* SingleListItem<KeyT, ValueT>::get_next() const
{
    return next_;
}

template <typename KeyT, typename ValueT>
void SingleListItem<KeyT, ValueT>::set_next(SingleListItem *next)
{
    next_ = next;
}


template <typename KeyT, typename ValueT>
void SingleListItem<KeyT, ValueT>::copy_from(const SingleListItem<KeyT, ValueT> *src)
{
    key_ = src->get_key();
    value_ = src->get_value();
    next_ = src->get_next();
}

#endif // SINGLE_LIST_ITEM_IMPL_H
