#ifndef B_S_TREE_H
#define B_S_TREE_H

#include "BSTreeNode.h"

template<typename KeyT, typename ValueT>
class BSTree
{
  private:
    BSTreeNode<KeyT, ValueT> *root_ = nullptr;

  public:
    /* Insert new node into tree */
    void insert(const KeyT &key, const ValueT &value);
    /* Erase node with given key from tree */
    void erase(const KeyT &key);
    /* Check if tree contains node with given key */
    bool has_key(const KeyT &key) const;
    /* Get value of node with given key */
    const ValueT& get(const KeyT &key) const;
    /* Get pointer to root node */
    BSTreeNode<KeyT, ValueT>* get_root() const;
    /* Print values of all nodes ordered according to key */
    void print(BSTreeNode<KeyT, ValueT> *root);
};

#include "BSTree-impl.h"

#endif // B_S_TREE_H
